package objects;
//!----------creating employee with experience------------
    class Worker extends User {

    Double experience;
    Double salary;

    public Worker(String firstName, String lastName, String phone, String mobilePhone, String email,
                  String password, Double salary, Double experience){
        super(firstName, lastName, phone, mobilePhone, email, password);
       this.setSalary(salary);
       this.setExperience(experience);
    }
    public void RaiseSalary (){
        if(experience < 2){
            this.salary = salary + (salary*0.02);
        }
        else if(experience >=2 && experience <=5){
            this.salary = salary + (salary*0.1);
        }
        else if(experience >5){
            this.salary = salary +(salary*0.15);
        }
    }

    public Double getExperience() {
        return experience;
    }
    public void setExperience(Double experience) {
        this.experience = experience;
    }
    public Double getSalary() {
        return salary;
    }
    public void setSalary(Double salary) {
        this.salary = salary;
    }

    @Override
    public void PrintUserData() {
        super.PrintUserData();
        System.out.println("Salary:"+salary);
    }
}

