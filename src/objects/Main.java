
package objects;

public class Main {
    public static void main(String[]args){
        System.out.println("John Doe' user data:");
        User user1 = new User("John",
                "Doe",
                "12345",
                "380501111111",
                "John@mail.ru",
                "Test12345");
        user1.PrintUserData();
        System.out.println('\n');

        System.out.println("Creating user with email and password required:");
        User user2 = new User("jane@yandex.ru",
                "Test54321"); //Only email and password required
        user2.PrintUserData();
        System.out.println('\n');
        System.out.println("Jane Doe' user data:");
        user2.setFirstName("Jane");
        user2.setLastName("Doe");
        user2.setPhone("54321");
        user2.setMobilePhone("380502222222");
        user2.setEmail("jane@yandex.ru");
        user2.setPassword("Test54321");
        user2.PrintUserData();
        System.out.println();

        Worker employee = new Worker("Mark",
                "Green",
                "55555",
                "380505555555",
                "director@gmail.ru",
                "TESt12345",
                2500.00,
                2.0);
        System.out.println("Worker data:");
        employee.PrintUserData();
        employee.RaiseSalary();
        System.out.println("\n");
        System.out.println("Updating worker's data (salary)" );
        employee.PrintUserData();
        System.out.println("\n");
        System.out.println("Updating worker's data (experience and salary(if necessary))" );
        employee.setExperience(9.0);
        employee.RaiseSalary();
        employee.PrintUserData();


    }

}
