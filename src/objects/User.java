package objects;

public class User {
    private String firstName;
    private String lastName;
    private String phone;
    private String mobilePhone;
    private String email;
    private String password;

    //Creating user with all fields required
    public User(String firstName,String lastName,String phone,String mobilePhone,String email,String password) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.phone = phone;
        this.mobilePhone = mobilePhone;
        this.setEmail(email);
        this.setPassword(password);
    }
    //Creating user with fields email and password
    public User(String email,String password){
        this.firstName ="";
        this.lastName ="";
        this.phone ="";
        this.mobilePhone ="";
        this.setEmail(email);
        this.setPassword(password);
    }

//    private boolean checkEmail(String email){
//        return email.contains("@");
//    }
//
//    private boolean checkPassword(String password){
//        if (password.length() >=8 && password.length() <= 16){
//            return true;
//        }
//        else return false;
//    }
    public void PrintUserData(){

        System.out.println("FirstName:"+firstName+", lastName:"+lastName+", phone:"+phone+", " +
                "mobilePhone:"+mobilePhone+", email:"+email+", Password:"+password);
//

    }
    public String getFirstName() {
        return firstName;
    }
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }
    public String getLastName() {
        return lastName;
    }
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }
    public String getPhone() {
        return phone;
    }
    public void setPhone(String phone) {
        this.phone = phone;
    }
    public String getMobilePhone() {
        return mobilePhone;
    }
    public void setMobilePhone(String mobilePhone) {
        this.mobilePhone = mobilePhone;
    }
    public String getEmail() {
        return email;
    }


    public void setEmail(String email) {
        try{
            if(email.contains("@")){
                this.email = email;
            } else {
                throw new Exception("Check your email.");
            }
        } catch (Exception e){
            System.out.println(e);
        }
    }


    public String getPassword() {
        return password;
    }

    public void setPassword(String password){
        try{
            if(password.length() >= 8 && password.length() <= 16){
                this.password = password;
            } else {
                throw new ArithmeticException("Correct password must contain from 8 to 16 symbols");
            }
        } catch (ArithmeticException e){
            System.out.println(e);
        }


    }
}
